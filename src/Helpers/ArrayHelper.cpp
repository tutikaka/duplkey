#include "Arduino.h"

void static copyAddress(const byte *source, byte *purpose) {
    for (int8_t i = 0; i < 8; i++) {
        purpose[i] = source [i];
    }
};

bool static compareAddress(const byte *left, const byte *right) {
    for (int8_t i = 7; i >= 0; i--) {
        if (left[i] != right[i]) {
            return false;
        }
    }
    return true;
};
