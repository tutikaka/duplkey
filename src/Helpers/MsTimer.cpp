#include <Arduino.h>

/**
 * Millisecond timer.
 */
class MsTimer {
private:
    bool oneSecondUpdateFlag = false;
    unsigned long counter;
    unsigned long lapTempTime;

public:
    explicit MsTimer (unsigned long millisecond) {
        lapTempTime = millisecond;
        counter = millisecond;
    }

    bool check() {
        if (millis() - lapTempTime > counter) {
            oneSecondUpdateFlag = !oneSecondUpdateFlag;
            lapTempTime = millis();
        }
        return oneSecondUpdateFlag;
    }
};
