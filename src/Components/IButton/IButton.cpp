#define STATUS_WAIT 200
#define STATUS_SUCCESSFULLY 201
#define STATUS_ERROR_NULL 203
#define STATUS_ERROR_IDENTICAL 209

#include "OneWire.h"
#include "Helpers/ArrayHelper.cpp"

/**
 * Работа с ключами типа IButton, на основе OneWire библиотеки.
 *
 * Коды статусов и ошибок.
 * - запись:
 * -- 200 - STATUS_WAIT- ожидание
 * -- 201 - STATUS_SUCCESSFULLY - запись прошла удачно
 * -- 203 - STATUS_ERROR_NULL - ошибка, записываемый адрес равен нулю
 * -- 209 - STATUS_ERROR_IDENTICAL - ошибка, адреса перед записью и после совпадают
 */
class IButton {
private:
    byte pinNumber;
    OneWire key;
    byte address[8]{},
         oldAddress[8]{},
         verificationAddress[8]{};

    int writeAddress(const byte *newAddress) {
        if (key.search(oldAddress)) {
            key.skip();
            key.reset();
            key.write(0x33); // чтение текущего номера ключа

            key.skip();
            key.reset();
            key.write(0xD1); // команда разрешения записи

            digitalWrite(pinNumber, LOW);
            pinMode(pinNumber, OUTPUT);
            delayMicroseconds(60);
            pinMode(pinNumber, INPUT);
            digitalWrite(pinNumber, HIGH);
            delay(10);

            key.skip();
            key.reset();
            key.write(0xD5); // команда записи
            for (byte i = 0; i < 8; i++) {
                writeByte(newAddress[i]);
            }

            key.reset();
            key.write(0xD1); // команда выхода из режима записи
            digitalWrite(pinNumber, LOW);
            pinMode(pinNumber, OUTPUT);
            delayMicroseconds(10);
            pinMode(pinNumber, INPUT);
            digitalWrite(pinNumber, HIGH);
            delay(10);

            delay(100);
            key.reset_search();
            delay(50);
            if (key.search(verificationAddress)) {
                if (compareAddress(verificationAddress, oldAddress)) {
//                    Serial.println(F("Write error! The old and new addresses are the same."));
                    return STATUS_ERROR_IDENTICAL;
                }
                if (compareAddress(verificationAddress, newAddress)) {
//                    Serial.println(F("Successfully!")); // Verification was successful: the current address of the key matches the one being recorded
                    return STATUS_SUCCESSFULLY;
                }
            }
        }

        return STATUS_WAIT;
    }

    void writeByte(byte data) const {
        for(byte data_bit=0; data_bit<8; data_bit++) {
            if (data & 1) {
                digitalWrite(pinNumber, LOW);
                pinMode(pinNumber, OUTPUT);
                delayMicroseconds(60);
                pinMode(pinNumber, INPUT);
                digitalWrite(pinNumber, HIGH);
                delay(10);
            } else {
                digitalWrite(pinNumber, LOW);
                pinMode(pinNumber, OUTPUT);
                pinMode(pinNumber, INPUT);
                digitalWrite(pinNumber, HIGH);
                delay(10);
            }
            data = data >> 1;
        }
    }

public:
    explicit IButton(unsigned int pin) {
        key.begin(pin);
        pinNumber = pin;
    }

    void listen() {
        if (key.search(address)) { // Если устройство подключено - считываем
            key.reset_search(); // Сбрасываем устройство
        }
    }

    int write() {
        return writeAddress(address);
    }

    bool checkCRC() {
        return OneWire::crc8(address, 7) == address[7];
    };

    /**
     * Конвертировать адрес из массива в строку.
     *
     * @param addressArray
     * @param crcOutput Если true, то выводится последний байт,
     * который является контрольной суммой.
     * @param longOutput Если true, то к значениям байтов, которых
     * в десятичной системе имеют один символ, вначале будет добавлен ноль,
     * например: адрес 1 29 0 2F 0 5 F C8
     * будет заменен на 01 29 00 2F 00 05 0F C8
     *
     * @return String
     */
//    static String convertAddressToString(byte *addressArray, bool crcOutput = true, bool longOutput = false) {
//        String addressContainer = "";
//
//        byte outputLength = 7;
//        if (crcOutput) {
//            outputLength = 8;
//        }
//
//        for (byte i = 0; i < outputLength; i++) // Запускаем цикл печати данных из массива
//        {
//            String tmpByteContainer = (String) addressArray[i];
//
//            if (longOutput) {
//                if (tmpByteContainer.length() <= 1) {
//                    addressContainer += "0" + tmpByteContainer;
//                } else {
//                    addressContainer += tmpByteContainer;
//                }
//            } else {
//                addressContainer += tmpByteContainer;
//            }
//
//            if (i < 7) {
//                addressContainer += " ";
//            }
//        }
//
//        return addressContainer;
//    }

    void addressReset() {
        for (unsigned char & addressByte : address)
        {
            addressByte = 0;
        }
    }

    void addressOldReset() {
        for (unsigned char & addressByte : oldAddress)
        {
            addressByte = 0;
        }
    }

    void reset() {
        key.reset_search();
    }

    byte *getAddress() {
        return address;
    }

    byte *getOldAddress() {
        return oldAddress;
    }
};
