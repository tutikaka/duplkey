#define BTN_PIN 3
#define IBUTTON_PIN 4

#include "Helpers/definitions.cpp"
#include <Arduino.h>
#include "GyverOLED.h"
#include <EncButton.h>
#include "Components/IButton/IButton.cpp"
#include "Helpers/MsTimer.cpp"

GyverOLED<SSD1306_128x64, OLED_BUFFER> oled;
IButton ibutton(IBUTTON_PIN);
EncButton<EB_TICK, BTN_PIN> butt1;
MsTimer oneSecond(1000);

bool rwFlag = true; //чтение(true) или запись(false)

const char *layoutTitle[2]  = {
        "READ",
        "WRITE",
};

const char *layoutStatus[3]  = {
        "waiting...",
        "Done",
        "Error",
};

const char *crcStatus[2]  = {
        "CRC: ok",
        "CRC: error!",
};

int writeStatus = 200;


void buttonCheckList();
void printStatusBar(const char*& label);
void powerOnStatus();
void printAddress(byte *adr, bool crcOutput = true);
void printDescription(IButton iKey);
void oledPrintInvert (const char*& text);
void navigator(
        byte x = 105,
        byte y = 6,
        byte distanceCenter = 5,
        byte distanceTriangle = 9
);

void triangle(
        byte incomingX,
        byte incomingY,
        byte height,
        bool orientation = true,
        byte fill = 1
);

void setup() {
    oled.init();
    Wire.setClock(800000L);   // настройка скорости I2C, макс. 800'000
}

void loop()
{
    buttonCheckList();
    oled.clear();

    //если чтение
    if (rwFlag) {
//        layoutTitle = "READ";
        ibutton.listen();
        byte *address = ibutton.getAddress();
        ibutton.reset();

        printAddress(address);
        oled.println();
        oled.println();

        if (address[7] != 0) {
            printDescription(ibutton);
        }
    }

    //если запись
    if (!rwFlag) {
        byte *address = ibutton.getAddress();
        ibutton.reset();

        oled.print("New address:\r\n");
        printAddress(address);
        oled.println();

        if (writeStatus == STATUS_WAIT){
            writeStatus = ibutton.write();
        }

        byte *oldAddress = ibutton.getOldAddress();
        if ((oldAddress[7] != 0) & (writeStatus != 600)) {
            oled.print("\r\nOld address:\r\n");
            printAddress(oldAddress);
        } else {
            oled.print("\r\nExpectation key \r\nfor write...\r\n");
        }

        if (writeStatus == STATUS_WAIT)
            printStatusBar(layoutStatus[0]);
        if (writeStatus == STATUS_SUCCESSFULLY)
            printStatusBar(layoutStatus[1]);
        if (writeStatus == STATUS_ERROR_IDENTICAL)
            printStatusBar(layoutStatus[2]);
    }

    oled.update();
}

void buttonCheckList() {
    butt1.tick();
    if(butt1.click()) {
        //смена записи и чтения
        rwFlag = !rwFlag;
        writeStatus = STATUS_WAIT;
    }

    if(butt1.held()) { //удержание
        if (rwFlag) {
            //сбросить адрес
            ibutton.addressReset();
        }

        if (!rwFlag) {
            //сбросить адрес
            writeStatus = STATUS_WAIT;
            ibutton.addressOldReset();
        }
    }
}

void printStatusBar(const char*& label) {
    oled.home();    // курсор в 0,0
    powerOnStatus();

    oled.setCursorXY(15, 0);
    oled.print(label);
    oled.fastLineH(15, 0, 128);

//    navigator();

    oled.fastLineH(15, 0, 128);

    oled.textMode(BUF_ADD);
    oled.autoPrintln(true);
    oled.setCursorXY(0, 16);
}

void powerOnStatus() {
    if (oneSecond.check()){
        oled.circle(7, 4, 2, OLED_FILL);
    } else {
        oled.circle(7, 4, 2, OLED_STROKE);
    }
}

void oledPrintInvert (const char*& text) {
    oled.invertText(INVERT_TEXT);
    oled.print(text);
    oled.invertText(INVERT_TEXT_DEFAULT);
}

void printAddress(byte *adr, bool crcOutput) {
    for (byte i = 0; i < 8; i++) // Запускаем цикл печати данных из массива
    {
        if (i < 7) {
            oled.print(adr[i], HEX);
            oled.print(F(" "));
        }

        // Печатаем crc в инвертированых цветах
        if ((i == 7) & crcOutput) {
            oled.invertText(INVERT_TEXT);
            oled.print(adr[7], HEX);     // Печатаем crc
            oled.invertText(INVERT_TEXT_DEFAULT);
        }
    }
}

void printDescription(IButton iKey) {
    if ( iKey.checkCRC() ) { // проверяем контрольную сумму приложенного ключа
        oled.print(F("CRC: ok."));
    } else {
        oledPrintInvert(crcStatus[1]);
    }
}

void navigator(
        byte x,
        byte y,
        byte distanceCenter,
        byte distanceTriangle
) {
    triangle(x - distanceCenter - distanceTriangle, y, 5, false);
    triangle(x - distanceCenter, y , 9, false);

    oled.fastLineV(x, y - 4, y + 4);

    triangle(x + distanceCenter, y , 9);
    triangle(x + distanceCenter + distanceTriangle, y, 5);
}

void triangle(
        byte incomingX,
        byte incomingY,
        byte height,
        bool orientation,
        byte fill
        ) {
    byte beginY = incomingY - height / 2;
    byte endY = beginY + height;

    //вертикальное основание
    oled.fastLineV(incomingX, beginY, endY);

    int8_t increment = 1;
    if (!orientation) {
        increment = -1;
    }

    //грани
    byte x = incomingX + increment;
    for (int y = beginY; y <= endY; y++) {
        oled.dot(x, y);

        if (y < (height / 2) + beginY) {
            x += increment;
        } else {
            x -= increment;
        }
    }
}
